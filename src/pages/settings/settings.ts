import { Component} from '@angular/core';
import { AlertController,MenuController } from 'ionic-angular';

@Component({
    selector: 'page-settings',
    templateUrl: 'settings.html'
})

export class SettingsPage {
    constructor(private alertCtrl: AlertController,
                private menuCtrl:MenuController){}

    onToggleLights(){
        let alert = this.alertCtrl.create({
            title: 'Il va faire tout noir ?!',
            subTitle: 'Sinon tu peux annuler aussi ',

        buttons: [
            {
                text: 'Annuler',
                role: 'cancel'
            },
            {
                text: 'Confirmer',
                handler: () => console.log('Confimer!')
            }
        ]
        });
        alert.present();
    }

    onToggleMenu(){
        this.menuCtrl.open();
    }
}
