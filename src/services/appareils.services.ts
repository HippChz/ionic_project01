import { Appareil } from '../models/Appareil';

export class AppareilsService {

    appareilsList: Appareil[] = [
        {
            name: 'Machine à laver',
            description: [
                'Volume: 6 litres',
                'Temps de lavage: 2 heures',
                'Consomation: 173kWh/an'
            ],
            isOn: true
        },
        {
            name: 'Télévision',
            description: [
                'Dimensions: 40 pouces',
                'Consommation: 22 kWh/an'
            ],
            isOn: true
        },
        {
            name: 'Ordinateur',
            description: [
                'Marque: Fait maison',
                'Consomation: 500 kWh/an'
            ],
            isOn: false
        }
    ];
}
